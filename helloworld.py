
import random

myscore = 0
aiscore = 0

choices = ["r","p","s"]

max_points = 3

while myscore < max_points and aiscore < max_points:
    player_choice = input("Please select a hand: ")
    ai_choice = random.choice(choices)

    if player_choice == ai_choice:
        print("tie!")

    elif ai_choice=="r" and player_choice =="s" or ai_choice=="s" and player_choice =="p" or ai_choice=="p" and player_choice =="r":
        print("AI wins")
        aiscore +=1

    else:
        print("you win!")
        myscore+=1

print (myscore)
print (aiscore)

